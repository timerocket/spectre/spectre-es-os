// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process unless
// nodeIntegration is set to true in webPreferences.
// Use preload.js to selectively enable features
// needed in the renderer process.
const path = require('path');
const fs = require('fs');
const envFilePath = path.join(process.resourcesPath, '.env');
if (fs.existsSync(envFilePath)) {
    require('dotenv').config({path: envFilePath});
    //console.log(`RUNTIME=${process.env.RUNTIME}`);
} else {
    //console.log(`envFilePath: ${envFilePath} does not exist`)
}

if(process.env.RUNTIME === 'win'){
    require('win-ca');
}

const ejs = require("ejs");
const Router = require("./lib/Router");
const ConnectController = require("./lib/module/ConnectController");
const IndexViewController = require("./lib/module/IndexViewController");
const ElementViewController = require("./lib/module/ElementViewController");
const ElasticDriver = require("./lib/ElasticDriver");
const Modal = require("./lib/Modal");
const LicenseKey = require("./lib/LicenseKey");
const filePath = require("./lib/FilePath");


const router = new Router();
const connect_controller = new ConnectController(router);
const index_view_controller = new IndexViewController(router);
const view_elements_controller = new ElementViewController(router);


connect_controller.subscribe("connect", function () {
    router.elasticDriver = connect_controller.elasticDriver;
    router.route(index_view_controller);
});

connect_controller.subscribe("check-status", function () {
    document.getElementById("connection_status").innerHTML = `<i class="fas fa-fw fa-spin fa-spinner"></i>`;
});

connect_controller.subscribe("check-status-bad", function (data) {
    document.getElementById("connection_status").innerHTML = `<i style="color:#e25353;" class="fas fa-fw fa-exclamation-circle"></i><span class="tooltip">${data}</span>`;
});

connect_controller.subscribe("check-status-good", function () {
    document.getElementById("connection_status").innerHTML = `<i style="color:#8ae264;" class="fas fa-fw fa-check-circle"></i>`;
});

connect_controller.subscribe("check-status-reset", function () {
    document.getElementById("connection_status").innerHTML = `<i class="fas fa-fw fa-ellipsis-h"></i>`;
});

connect_controller.subscribe("uri-reset", function () {
    document.getElementById("uri").style.backgroundColor = `#60728b`;
});

connect_controller.subscribe("uri-bad", function () {
    document.getElementById("uri").style.backgroundColor = `#e25353`;
});

view_elements_controller.subscribe("select-index", function (index) {
    view_elements_controller.setIndex(index);
    router.route(view_elements_controller);
});

index_view_controller.subscribe("select-index", function (index) {
    view_elements_controller.setIndex(index);
    router.route(view_elements_controller);
});

function disconnect() {
    router.elasticDriver.client.close();
    router.elasticDriver = new ElasticDriver();
    router.route(connect_controller);
}

router.subscribe("disconnect", disconnect);
index_view_controller.subscribe("disconnect", disconnect);

connect_controller.subscribe("refresh", ()=>{
    router.route(connect_controller);
})

view_elements_controller.subscribe("back", function () {
    router.route(index_view_controller);
});

// Open all links in external browser
let shell = require('electron').shell
document.addEventListener('click', function (event) {
    if (event.target.tagName === 'A' && event.target.href.startsWith('http')) {
        event.preventDefault()
        shell.openExternal(event.target.href)
    }
})


router.route(connect_controller);

function launchLicenseKeyModal() {
    const modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/license-key-modal.ejs")).toString(), {}, null));
    const activate_element = document.getElementById('activate');
    const license_element = document.getElementById('license-key');

    license_element.onclick = () => {
        license_element.style.backgroundColor = `#60728b`;
    }

    activate_element.onclick = async () => {
        let _key = license_element.value;
        if (_key.split("-").length !== 4 || _key.length !== ((8 * 4) + 3)) {
            license_element.style.backgroundColor = `#e25353`;
            return;
        }
        document.getElementById("license-key-form").style.display="none";
        document.getElementById("license-key-status").innerHTML=`<h4><i class="fas fa-2x fa-spin fa-circle-notch"></i><br/><br/>Authenticating License...</h4>`;
        let check = await licenseKey.checkAuthorized(_key);
        if (check){
            licenseKey.consumeKey(_key);
            modal.close();
        }else{
            document.getElementById("license-key-form").style.display="block";
            document.getElementById("license-key-error-message").innerHTML = `<p class="error">Invalid License Key</p>`;
            document.getElementById("license-key-status").innerHTML = "";
        }
    }
}

const licenseKey = new LicenseKey();
if (!licenseKey.isAuthorized()) {
    launchLicenseKeyModal()
}
