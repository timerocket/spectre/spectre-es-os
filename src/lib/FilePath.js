const path = require("path");

function filePath(relative) {
    return path.join(__dirname, relative).replace(/\\/g, '\\\\')
}

module.exports = filePath;