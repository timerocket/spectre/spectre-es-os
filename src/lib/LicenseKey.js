const https = require('https')

const licenseVerificationOptions = {
    hostname: 'api.gumroad.com',
    port: 443,
    path: '/v2/licenses/verify',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': 0
    }
}

class LicenseKey {


    constructor() {
        this.user_key = window.localStorage.getItem("_spectre_es_gui_user_key");
    }

    isAuthorized() {
        if (process.env.RUNTIME === "mas") {
            return true;
        }

        if (this.user_key && this.user_key.length === ((8 * 4) + 3)) {
            return true;
        }
    }

    async checkAuthorized(key) {

        return new Promise((resolve) => {

            licenseVerificationOptions.path = `/v2/licenses/verify?product_permalink=spectre-elasticsearch-gui&license_key=${key}&increment_uses_count=false`;
            let data = "";
            const req = https.request(licenseVerificationOptions, res => {
               // console.log(`statusCode: ${res.statusCode}`)
                res.on('data', d => {
                    if (res.statusCode === 200) {
                        data += d.toString();
                    }
                })

                res.on('end', error => {
                    if (data.length === 0){
                        resolve(false);
                        return;
                    }
                    let license_data = JSON.parse(data);
                    //console.log(license_data);
                    if (license_data.uses < license_data.purchase.quantity * 25 && !license_data.purchase.refunded) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                })
            })


            req.on('error', error => {
             //   console.error(error)
                resolve(false);
            })

            req.end();

        });

    }

    revokeKey() {
        this.user_key = null;
        window.localStorage.removeItem("_spectre_es_gui_user_key");
    }

    consumeKey(key) {

        return new Promise((resolve) => {

            licenseVerificationOptions.path = `/v2/licenses/verify?product_permalink=spectre-elasticsearch-gui&license_key=${key}&increment_uses_count=true`;
            const req = https.request(licenseVerificationOptions, (res) => {
                this.user_key = key;
                window.localStorage.setItem("_spectre_es_gui_user_key", key);
                resolve(true);
            })

            req.on('error', error => {
                resolve(false);
            })

            req.end();

        });
    }

}

module.exports = LicenseKey;