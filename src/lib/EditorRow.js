const unflattenObject = require("./UnflattenObject");
const TypeFromProperty = require("./TypeFromProperty");

class EditorRow {
    source;
    source_field_types;
    subscribers = [];

    constructor(source) {
        this.source = source;
        this.source_field_types = [];
        for (const prop in this.source) {
            this.source_field_types[prop] = TypeFromProperty(this.source[prop]);
        }
    }

    getElement(available_actions, headers) {
        const row_elem = document.createElement("tr");
        let action_container_td = document.createElement("td");
        action_container_td.className = "actions";
        action_container_td.colSpan = available_actions.length;
        let action_container_td_table = document.createElement("table");
        let action_container_td_table_tr = document.createElement("tr");
        row_elem.className = "editing";

        const done_elem = document.createElement("span");

        done_elem.innerHTML = `<i class="fas fa-fw fa-check-circle"></i>`;
        done_elem.style.marginRight = "0.5em";
        done_elem.onclick = async (e) => {
            e.preventDefault();
            e.cancelBubble = true;


            for (const prop of headers) {
                if (prop == "_id") {
                    continue;
                }
                if ((this.source[prop] || "undefined").toString() === this.source_value_elements[prop].innerText) {
                    continue;
                }
                if (this.source_field_types[prop] !== "boolean") {
                    this.source_field_types[prop] = TypeFromProperty(this.source_value_elements[prop].innerText);
                }

                switch (this.source_field_types[prop]) {
                    case "string":
                        this.source[prop] = this.source_value_elements[prop].innerText;
                        break;
                    case "number":
                        let parsed_string_number = this.source_value_elements[prop].innerText.match(/[-]{0,1}[\d]*[.]{0,1}[\d]+/g);
                        if (!parsed_string_number) {
                            parsed_string_number = null;
                        } else {
                            parsed_string_number = parseFloat(parsed_string_number[0]);
                        }
                        this.source[prop] = parsed_string_number;
                        break;
                    case "boolean":
                        this.source[prop] = ((this.source_value_elements[prop].value === "1") || (this.source_value_elements[prop].innerText === "true"));
                        break;
                }
                if (this.source_value_elements[prop].innerText.trim() === "null") {
                    this.source[prop] = null;
                }
                if (this.source_value_elements[prop].innerText.trim() === "") {
                    delete this.source[prop];
                }

            }

            this.publish("done", this.rollupDocument(this.source));
        };


        const cancel_elem = document.createElement("span");
        cancel_elem.innerHTML = `<i class="fas fa-fw fa-undo"></i>`;
        cancel_elem.style.marginLeft = "0.5em";
        cancel_elem.onclick = async (e) => {
            e.preventDefault();
            e.cancelBubble = true;
            this.publish("cancel");
        };


        if (available_actions.indexOf("view") !== -1) {
            const view_elem = document.createElement("td");
            view_elem.className = "clickable";
            view_elem.innerHTML = `<i class="far fa-fw fa-file-alt"></i>`;
            view_elem.onclick = async (e) => {
                let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/view-document.ejs")).toString(), {
                    headers: headers,
                    doc: unflattenObject(this.source)
                }, null), {class: "wide"});
                let close_element = document.getElementById('cancel');
                close_element.onclick = () => {
                    modal.close();
                }
            }

            action_container_td_table_tr.appendChild(view_elem);
        }


        let elem_actions_container = document.createElement("td");
        elem_actions_container.className = "padded";
        elem_actions_container.className = "clickable";
        elem_actions_container.style.whiteSpace = "pre";
        elem_actions_container.appendChild(done_elem);
        elem_actions_container.appendChild(cancel_elem)
        if (available_actions.indexOf("delete") !== -1) {
            elem_actions_container.appendChild(document.createElement("td"))
        }

        action_container_td_table_tr.appendChild(elem_actions_container);
        action_container_td_table.append(action_container_td_table_tr);
        action_container_td.append(action_container_td_table);
        row_elem.append(action_container_td);

        this.source_value_elements = [];

        for (const prop of headers) {

            let edit_row_element = document.createElement("td");
            this.source_value_elements[prop] = edit_row_element;

            if (this.source_field_types[prop] === "boolean") {
                let select = document.createElement('select');
                select.innerHTML = `<option value="1" ${!!this.source[prop] ? "selected" : ""}>true</option><option value="0" ${!this.source[prop] ? "selected" : ""}>false</option>`;
                edit_row_element.appendChild(select);
                this.source_value_elements[prop] = select;
            } else {
                edit_row_element.innerText = this.source[prop] || "";
            }
            edit_row_element.className = "padded";
            if (prop !== "_id" && this.source_field_types[prop] !== "boolean") {
                edit_row_element.contentEditable = true;
            }
            row_elem.appendChild(edit_row_element);

        }


        return row_elem;
    }

    // Thanks to https://stackoverflow.com/users/4620771/nenad-vracar
    rollupDocument() {
        var result = {}
        for (var i in this.source) {
            var keys = i.split('.')
            keys.reduce((r, e, j) => {
                return r[e] || (r[e] = isNaN(Number(keys[j + 1])) ? (keys.length - 1 == j ? this.source[i] : {}) : [])
            }, result)
        }
        return result
    }

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }
}

module.exports = EditorRow;