function validateIndexName(name) {

    let result = {
        status:false,
        message:""
    };

    let invalid_characters = ['\\', '/', '*', '?', '"', '<', '>', '|', ' ', ','];

    if (name !== name.toLowerCase()) {
        result.message = `Index name must be lowercase`;
        return result;
    }

    for (var i = 0; i < invalid_characters.length; i++) {
        if (name.indexOf(invalid_characters[i]) > -1) {
            result.message = `Index name must not contain the following characters ${JSON.stringify(invalid_characters)}`;
            return result;
        }
    }

    if (name.indexOf("#") !== -1) {
        result.message = `Index name must not contain '#'`;
        return result;
    }
    if (name.indexOf(":") !== -1) {
        result.message = `Index name must not contain ':'`;
        return result;
    }
    if (name.charAt(0) === '_' || name.charAt(0) === '-' || name.charAt(0) === '+') {
        result.message = `Index name must not start with '_', '-', or '+'`;
        return result;
    }

    let byteCount = 0;
    try {
        byteCount = (new TextEncoder().encode(name)).length;
    } catch (UnsupportedEncodingException)
    {
        // UTF-8 should always be supported, but rethrow this if it is not for some reason
        result.message = `Unable to determine length of index name`;
        return result;
    }
    if (byteCount > 255) {
        result.message = `Index name is too long, ( ${byteCount} > 255 )`;
        return result;
    }

    if (index === (".") || index === ("..")) {
        result.message = `Index name must not be '.' or '..'`;
        return result;
    }
    result.status = true;
    return result;
}

module.exports = validateIndexName;