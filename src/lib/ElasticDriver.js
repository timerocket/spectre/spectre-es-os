const elasticsearch = require('elasticsearch');

class ElasticDriver {

    client;
    uri;
    _username;
    _password;
    _apiKey;
    _nickname;
    recentConnections = [];

    constructor() {
        let json_saved = window.localStorage.getItem('_spectre_es_gui_saved_connections');
        if (json_saved) {
            this.recentConnections = JSON.parse(json_saved);
        }
    }

    async testConnectionURI(uri) {

        try {

            let result = await fetch(uri);
            return await this.connectURI(uri);

        } catch (e) {

            return {status:false, message:"Connection Refused"};

        }

    }

    async connectURI(uri, config = null) {

        const client_config = {
            host: uri,
            log: 'trace'
        }

        this._username = document.getElementById('username').value;
        this._password = document.getElementById('password').value;
        this._apiKey = document.getElementById('apiKey').value;
        this._nickname = document.getElementById('save_connection_nickname').value;
        this.uri = uri;

        if (config) {
            this._username = config.username;
            this._password = config.password;
            this._apiKey = config.apiKey;
            this._nickname = config.nickname;
            this.uri = config.uri;
            client_config.host = config.uri;
        }

        if (this._apiKey) {
            client_config.httpAuth = {}; // DOES NOT WORK ? MAYBE REQUIRES SSL? UNKNOWN
            if (this._apiKey) {
                client_config.httpAuth.apiKey = this._apiKey;
            }
        }

        if (this._username || this._password) {
            const uri_break = this.uri.split("://");
            client_config.host = `${uri_break[0]}://${this._username}:${this._password}@${uri_break[1]}`;
        }

        this.client = await new elasticsearch.Client(client_config);


        return this.ping();

    }

    async getIndices() {
        let indx = await this.client.cat.indices({format: 'json'});

        return indx.sort((a,b)=>{
            if(a.index < b.index) { return -1; }
            if(a.index > b.index) { return 1; }
            return 0;
        });
    }

    async ping() {
        return new Promise((resolve, reject) => {

            if (!this.client) {
                resolve(false);
            } else {

                this.client.ping({}, async (error) => {
                    if (error) {
                        console.log(error, "Elasticsearch service could not be reached", "");
                        resolve({status: false, message: error.message});
                    } else {
                        console.log("Elasticsearch service was successfully pinged");
                        resolve({status: true})
                    }
                });
            }
        });
    }

    addRecentConnection(details) {
        let connection_details = Object.assign({}, this.getConnectionDetails());
        delete connection_details.recent_connections;
        delete connection_details.save_connection;

        let has_connection = false;

        if (!has_connection) {
            this.recentConnections.push(connection_details);
        }
        this.saveRecentConnections();
    }

    saveRecentConnections() {
        window.localStorage.setItem('_spectre_es_gui_saved_connections', JSON.stringify(this.recentConnections))
    }

    getConnectionDetails() {
        return {
            uri: this.uri,
            username: this._username,
            password: this._password,
            apiKey: this._apiKey,
            nickname: this._nickname,
            recent_connections: this.recentConnections,
            save_connection: null
        }
    }
}

module.exports = ElasticDriver;