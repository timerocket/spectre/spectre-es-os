const ejs = require("ejs");
const fs = require("fs");
const filePath = require("./FilePath");
const flatObject = require("./FlatObject");
const EditorRow = require("./EditorRow");
const DeletionRow = require("./DeletionRow");
const unflattenObject = require("./UnflattenObject");
const TypeFromProperty = require("./TypeFromProperty");

class ElasticSmartTable {

    table_container_element;
    app;
    elements_per_page = 10;
    page_number = 0;
    total_elements = 0;
    table_element;
    fixed_data_source;
    available_actions;
    collection_index;
    headers = [];
    query;
    sort_field;
    pagination_element;
    table_rows_selector_element;
    filter_field;
    match_selector;
    match_type;
    parsing_error;

    constructor(app, fixed_data_source, available_actions = [], index) {
        if (this.fixed_data_source) {
            this.updateFixedDataSource(fixed_data_source);
        } else {
            this.collection_index = index;
        }
        this.available_actions = available_actions;
        this.app = app;
        this.table_container_element = document.createElement('div');
        this.match_type = "fuzzy";
    }

    updateFixedDataSource(fixed_data_source) {
        this.fixed_data_source = fixed_data_source;
        this.total_elements = fixed_data_source.length;
    }

    async render(element) {

        this.table_container_element.innerHTML =
            ejs.render(fs.readFileSync(filePath("../view/smart-table.ejs")).toString(), {match_type: this.match_type}, null);

        element.appendChild(this.table_container_element);

        this.table_element = document.getElementById("smart-table-element");
        console.log("table inner", this.table_element.innerHTML);

        const search_input = document.getElementById("smart-table-filter");
        this.pagination_element = document.getElementById("smart-table-pagination");
        this.table_rows_selector_element = document.getElementById("smart-table-row-selector");

        let refresh_btn = document.getElementById("smart-table-refresh-rows");
        refresh_btn.onclick = () => {
            this.publish("refresh")
        }

        this.match_selector = document.getElementById("smart-table-exact-match");

        this.match_selector.onchange = async () => {
            this.match_type = this.match_selector.value;
            await this.clearTable();
            await this.updateTable();
        };

        if (this.query) {
            search_input.value = this.query;
        }


        search_input.onkeyup = async () => {

            this.query = search_input.value;
            await this.clearTable();
            await this.updateTable();
        }


        await this.updateTable();

    }

    async clearTable() {
        this.table_element.innerHTML = "";
        document.getElementById("smart-table-message").innerHTML = "";
    }

    async updateTable() {
        let data = await this.queryData();

        if (this.match_type === "exact" || this.match_type === "query") {
            document.getElementById("smart-table-filter-field").style.display = "none";
        } else {
            document.getElementById("smart-table-filter-field").style.display = "block";
        }

        if (data.length === 0) {
            document.getElementById("smart-table-message").innerHTML = ejs.render(fs.readFileSync(filePath("../view/smart-table-no-results.ejs")).toString(), {error: this.parsing_error}, null)
            return;
        }

        const headers = await this.updateHeadersFromRows(data);
        const rows = await this.renderRows(data);
        this.updateFilterField();
        await this.updatePagination();
    }

    updateFilterField() {

        const select_element = document.getElementById("smart-table-filter-select")
        select_element.innerHTML = "";

        for (let sel of ["*", ...this.headers]) {
            const option = document.createElement("option");
            option.innerText = sel;

            if (sel === this.filter_field) {
                option.selected = true;
            }

            select_element.appendChild(option);
        }

        select_element.onchange = async (e) => {
            this.filter_field = select_element.value;
            await this.clearTable();
            await this.updateTable();
        }


    }

    async updatePagination() {

        let row_selection = [10, 25, 50, 100, 250, 500];
        this.table_rows_selector_element.innerHTML = `<div class="flex-margin-right" style="width: max-content;"><small>Rows per page:</small><select id="smart-table-select-rows"></select></div>`;

        const select_element = document.getElementById("smart-table-select-rows");

        for (let sel of row_selection) {

            const option = document.createElement("option");
            option.innerText = sel;

            if (sel === this.elements_per_page) {
                option.selected = true;
            }

            select_element.appendChild(option);
        }

        select_element.onchange = async (e) => {
            this.elements_per_page = parseInt(select_element.value);
            await this.clearTable();
            await this.updateTable();
        }

        let display_page_number = this.page_number + 1;

        let pages = (((this.total_elements - 1) / this.elements_per_page) | 0) + 1;

        display_page_number = Math.min(display_page_number, pages);

        let page_links = ``;

        for (let i = Math.max(display_page_number - 2, 1); i <= Math.min(display_page_number + 2, pages); i++) {
            page_links += ` <div id="jump-page-${i}" class="page_item${display_page_number === i ? ' active' : ''}">${i}</div> `;
        }

        let prev = `<button ${display_page_number <= 1 ? 'disabled' : ''} rel="prev">Previous</button>`;
        let next = `<button ${display_page_number >= pages - 1 ? 'disabled' : ''} rel="next" style="margin-right: 8px;">Next</button>`;

        if (display_page_number > 1) {
            prev = `<button id="page-prev" rel="prev">Previous</button>`;
        }

        if (display_page_number < pages) {
            next = `<button id="page-next" rel="next" style="margin-right: 8px;">Next</button>`;

        }

        let secondary = `${prev}${page_links}${next}`;

        let pagecount = `<small>Page <span class="highlight">${display_page_number}</span> of <span class="highlight">${pages}</span></small>`;
        if (pages <= 1) {
            pagecount = "";
        }

        if (pages <= 1) {
            return this.pagination_element.innerHTML = `<div class="pagination noselect"><small>Page <span class="highlight">1</span> of <span class="highlight">1</span></small></div>`;
        }

        this.pagination_element.innerHTML = `<div class="pagination noselect">${pagecount}${secondary}</div>`;

        for (let i = Math.max(display_page_number - 2, 1); i <= Math.min(display_page_number + 2, pages); i++) {

            if (i !== display_page_number) {
                document.getElementById(`jump-page-${i}`).onclick = async () => {
                    this.page_number = i - 1;
                    await this.clearTable();
                    await this.updateTable();
                }
            }
        }

        if (display_page_number > 1) {
            let prev_element = document.getElementById("page-prev");
            prev_element.onclick = async () => {
                this.page_number--;
                await this.clearTable();
                await this.updateTable();
            }
        }
        if (display_page_number < pages) {
            let next_element = document.getElementById("page-next");
            next_element.onclick = async () => {
                this.page_number++;
                await this.clearTable();
                await this.updateTable();
            }
        }

    }

    getIconForHeader(prop, type) {
        prop = prop.toLowerCase();
        let numb_check = parseFloat(type);
        if (numb_check !== "NaN" && numb_check.toString() === type) {
            type = "number";
        } else {
            type = typeof type;
        }
        if (prop[0] === "_") {
            return "";
        }
        if (type === "number") {
            return `<i class="fas fa-hashtag"></i>&nbsp;`;
        }
        if (type === "boolean") {
            return `<i class="fas fa-toggle-on"></i>&nbsp;`;
        }
        if (prop === "created" || prop === "modified" || prop === "accessed" || prop === "registered") {
            return `<i class="far fa-calendar-alt"></i>&nbsp;`;
        }
        if (type === "string") {
            return `<i class="fas fa-align-left"></i>&nbsp;`;
        }
        return `<i class="fas fa-question-circle"></i>&nbsp;`;
    }

    async updateHeadersFromRows(data) {

        let header_row = document.getElementById("smart-table-header");
        if (!header_row) {
            header_row = document.createElement("tr");
            header_row.id = "smart-table-header";
            this.table_element.prepend(header_row);
        }

        let flat = {};
        data.forEach((item) => {
            flat = Object.assign(flat, flatObject(item));
            for (let prop in flat) {
                if (this.headers.indexOf(prop) === -1) {
                    //if (prop !== "_id") {
                    this.headers.push(prop);
                    //  }
                }
            }
        });

        if (this.available_actions.length) {
            const id = `header-actions`;
            if (!document.getElementById(id)) {

                const action_element_header = document.createElement("th");
                action_element_header.id = id;
                action_element_header.className = "actions";
                action_element_header.colSpan = this.available_actions.length;
                action_element_header.innerHTML = "<small>Row Actions</small>";
                //  let action_container_td_table = document.createElement("table");
                // let action_container_td_table_tr = document.createElement("tr");
                /* this.available_actions.forEach((key) => {
                     const id = `header-action-${key}`;
                     if (!document.getElementById(id)) {
                         const action_element_td = document.createElement("td");
                         action_element_td.id = id;
                         action_element_td.className = "action";
                         //action_element_td.innerHTML = `<small>${key}</small>`;
                         action_container_td_table_tr.appendChild(action_element_td);
                     }
                 });*/
                //  action_container_td_table.appendChild(action_container_td_table_tr)
                // action_element_header.appendChild(action_container_td_table);
                header_row.appendChild(action_element_header);
            }
        }

        let sorters = [];
        for (let prop of this.headers) {

            const id = `header-${prop}`;
            if (!document.getElementById(id)) {
                const header_element = document.createElement("th");
                header_element.id = id;
                header_element.innerHTML = this.getIconForHeader(prop, flat[prop]) + prop;

                const sorter = document.createElement("span");
                sorter.className = "sorter";
                sorter.innerHTML = `<i class="fas fa-fw fa-sort"></i>`;
                if (this.sort){
                    if (this.sort[prop]){
                        if (this.sort[prop].order === "desc"){
                            sorter.innerHTML = `<i class="fas fa-fw fa-sort-down"></i>`;
                        }else{
                            sorter.innerHTML = `<i class="fas fa-fw fa-sort-up"></i>`;
                        }
                    }
                }
                header_element.appendChild(sorter);

                sorters.push(sorter);

                sorter.onclick = async () => {
                    console.log("BEFORE CURRENT SORT", this.sort);
                    if (!this.sort || !this.sort[prop]) {
                        this.sort = {};
                        this.sort[prop] = {order: "desc", unmapped_type:"long"};
                        for (const _s of sorters) {
                            _s.innerHTML = `<i class="fas fa-fw fa-sort"></i>`;
                        }
                        sorter.innerHTML = `<i class="fas fa-fw fa-sort-down"></i>`;
                    } else {
                        if (this.sort[prop].order === "desc") {
                            this.sort[prop] = {order: "asc", unmapped_type:"long"};
                            sorter.innerHTML = `<i class="fas fa-fw fa-sort-up"></i>`;
                        } else if (this.sort[prop].order === "asc") {
                            this.sort = null;
                            sorter.innerHTML = `<i class="fas fa-fw fa-sort"></i>`;
                        }
                    }
                    console.log(this.sort);

                    let d = await this.queryData();
                    if (d.length === 0){

                        sorter.innerHTML = `<i class="fas fa-fw fa-sort"></i>`;

                        if (this.parsing_error.indexOf("Text fields are not optimised for operations that require per-document field data") !== -1){
                            this.parsing_error =  `Property [${prop}] was not correctly set up to be sorted on, this generally happens with text fields.<br /><br/>For more information about sorting and mapping, refer to the following documentation:<br /><a target="_blank" href="https://www.elastic.co/guide/en/elasticsearch/reference/current/sort-search-results.html#sort-search-results">https://www.elastic.co/guide/en/elasticsearch/reference/current/sort-search-results.html</a><br/><small>${this.parsing_error}</small>`;
                            //  this.sort = null;
                        }

                        let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/modal-notify.ejs")).toString(), {
                            data: `<p class="error">${this.parsing_error}</p>`,
                            title: "Sorting Error"
                        }, null), {class: "wide"});

                        let close_element = document.getElementById('close');
                        this.sort = null;
                        close_element.onclick = ()=>{
                            modal.close();
                        }

                    }else {
                        this.publish("refresh", {})
                    }
                }
                header_row.appendChild(header_element);

            }
        }


    }

    async renderRows(data) {


        for (const doc of data) {

            const id = `row-${doc._id || doc.index || "error"}`;
            if (this.table_element.querySelector("#"+id)) {
                continue;
            }
            const row_element = document.createElement("tr");
            row_element.className = "zebra";
            if (this.fixed_data_source) {
                row_element.className += " clickable";
            }
            row_element.innerHTML = ejs.render(fs.readFileSync(filePath("../view/smart-table-row.ejs")).toString(), {
                headers: this.headers,
                item: doc
            }, null);
            row_element.id = id;
            row_element.onclick = async () => {
                await this.publish("select", doc);
            }

            let action_elements = await this.attachActions(row_element, doc);
            if (action_elements.length) {
                let action_container_td = document.createElement("td");
                action_container_td.className = "actions";
                action_container_td.colSpan = action_elements.length;
                let action_container_td_table = document.createElement("table");
                let action_container_td_table_tr = document.createElement("tr");

                for (const action of action_elements) {
                    action_container_td_table_tr.appendChild(action);
                }

                action_container_td_table.appendChild(action_container_td_table_tr);
                action_container_td.appendChild(action_container_td_table);
                row_element.prepend(action_container_td)
            }
            this.table_element.appendChild(row_element);
        }

    }

    async attachActions(row_element, doc) {
        let elements = [];
        for (const key of this.available_actions) {
            switch (key) {

                case "delete":
                    const delete_row_element = document.createElement("td");
                    delete_row_element.className = "padded";
                    delete_row_element.innerHTML = `<i class="far fa-trash-alt"></i>`;

                    delete_row_element.onclick = async (e) => {
                        e.preventDefault();
                        e.cancelBubble = true;

                        row_element.style.display = "none";

                        const deletion_row = new DeletionRow(doc);
                        const deletion_row_elem = deletion_row.getElement(this.available_actions, this.headers);

                        row_element.parentNode.insertBefore(deletion_row_elem, row_element.nextSibling);

                        deletion_row.subscribe("delete", async (updated) => {
                            row_element.style.display = "table-row";
                            deletion_row_elem.parentNode.removeChild(deletion_row_elem);
                            row_element.parentNode.removeChild(row_element);
                            await this.publish("delete", updated);
                            await this.updateTable();
                        });

                        deletion_row.subscribe("cancel", async () => {
                            row_element.style.display = "table-row";
                            deletion_row_elem.parentNode.removeChild(deletion_row_elem);
                        });

                    };

                    elements.push(delete_row_element);
                    break;


                case "view":

                    const view_row_element = document.createElement("td");
                    view_row_element.className = "clickable";
                    view_row_element.innerHTML = `<i class="far fa-file-alt"></i>`;

                    view_row_element.onclick = async (e) => {
                        let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/view-document.ejs")).toString(), {
                            headers: this.headers,
                            doc: unflattenObject(doc)
                        }, null), {class: "wide"});
                        let close_element = document.getElementById('cancel');
                        close_element.onclick = () => {
                            modal.close();
                        }
                    }

                    elements.push(view_row_element);

                    break;

                case "edit":
                    const edit_row_element = document.createElement("td");
                    edit_row_element.className = "padded";
                    edit_row_element.style.textAlign = "center";
                    edit_row_element.style.whiteSpace = "nowrap";


                    let edit_row_icon_inline = document.createElement("i");
                    edit_row_icon_inline.className = "fas fa-fw fa-edit";
                    edit_row_icon_inline.style.marginRight = "0.5em";

                    let edit_row_icon_code = document.createElement("i");
                    edit_row_icon_code.className = "fas fa-fw fa-code";
                    edit_row_icon_code.style.marginLeft = "0.5em";

                    edit_row_element.appendChild(edit_row_icon_inline);
                    edit_row_element.appendChild(edit_row_icon_code);

                    edit_row_icon_inline.onclick = async (e) => {
                        e.preventDefault();
                        e.cancelBubble = true;

                        row_element.style.display = "none";

                        const editor_row = new EditorRow(doc);
                        const editor_row_elem = editor_row.getElement(this.available_actions, this.headers);

                        row_element.parentNode.insertBefore(editor_row_elem, row_element.nextSibling);

                        editor_row.subscribe("done", async (updated) => {
                            row_element.style.display = "table-row";
                            editor_row_elem.parentNode.removeChild(editor_row_elem);
                            row_element.innerHTML = ejs.render(fs.readFileSync(filePath("../view/smart-table-row.ejs")).toString(), {
                                headers: this.headers,
                                item: flatObject(updated)
                            }, null);

                            let action_elements = await this.attachActions(row_element, doc);
                            if (action_elements.length) {
                                let action_container_td = document.createElement("td");
                                action_container_td.className = "actions";
                                action_container_td.colSpan = action_elements.length;
                                let action_container_td_table = document.createElement("table");
                                let action_container_td_table_tr = document.createElement("tr");

                                for (const action of action_elements) {
                                    action_container_td_table_tr.appendChild(action);
                                }

                                action_container_td_table.appendChild(action_container_td_table_tr);
                                action_container_td.appendChild(action_container_td_table);
                                row_element.prepend(action_container_td)
                            }


                            await this.publish("edit", updated);
                            await this.updateTable();
                        });

                        editor_row.subscribe("cancel", async () => {
                            row_element.style.display = "table-row";
                            editor_row_elem.parentNode.removeChild(editor_row_elem);
                        });

                    };

                    const edit_document_modal_fn = (err, edit) => {

                        let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/edit-document-json.ejs")).toString(), {
                            doc: edit||JSON.stringify(unflattenObject(doc),null,"\t"),
                            err: err || null
                        }, null), {class: "wide"});

                        let document_element = document.getElementById('document');
                        let save_element = document.getElementById('save');
                        let cancel_element = document.getElementById('cancel');

                        document_element.onclick = () => {
                            document_element.style.backgroundColor = "#2f3a4b";
                        }

                        let insert_json_data, raw_json_data, raw_input_data;

                        save_element.onclick = async () => {
                            raw_input_data  = document_element.value;
                            let valid_json = false;
                            try {

                                insert_json_data = JSON.parse(raw_input_data);
                                raw_json_data = JSON.parse(raw_input_data);
                                valid_json = true;
                            } catch (e) {
                                console.error(e);
                                modal.close();
                                edit_document_modal_fn(e.message, raw_input_data);
                            }

                            if (!document_element.value || !valid_json) {
                                document_element.style.backgroundColor = `#e25353`;
                                return false;
                            }
                            modal.modal_window.style.textAlign = "center";
                            modal.modal_window.style.color = "#c7d7ef";
                            modal.modal_window.innerHTML = `<i class="fas fa-4x fa-spin fa-circle-notch"></i><h4>Inserting Document${(insert_json_data instanceof Array) ? "s" : ""}...</h4>`;

                            let update_id;

                            try {
                                let remove_props = [];
                                for (let prop in doc) {
                                    if (insert_json_data[prop] === undefined || insert_json_data[prop] === null) {
                                        remove_props.push(prop);
                                        delete insert_json_data[prop];
                                    }
                                }

                                update_id = insert_json_data._id;
                                delete insert_json_data._id;

                                let query = {
                                    index: this.collection_index.index,
                                    id: update_id,
                                    refresh: true,
                                    body: {
                                        doc: insert_json_data
                                    }
                                };

                                const result = await this.app.elasticDriver.client.update(query);

                                if (remove_props.length > 0) {
                                    delete query.body.doc;
                                    query.body.script = {
                                        source: ""
                                    }
                                    for (let field of remove_props) {
                                        query.body.script.source += `ctx._source.remove('${field}');`;
                                    }
                                    const result_remove = await this.app.elasticDriver.client.update(query);

                                }
                                this.publish("refresh");
                            } catch (e) {
                                modal.close();
                                let err_msg = e.message;
                                if (doc._id !== raw_json_data._id || err_msg.indexOf("document_missing_exception") !== -1){
                                    err_msg = `You cannot change the _id of an existing document. _id has been reverted.`;
                                    raw_json_data._id = doc._id;
                                }
                                edit_document_modal_fn(err_msg, raw_input_data);
                                return;
                            }

                            modal.close();
                        }

                        cancel_element.onclick = () => {
                            modal.close();
                        }

                    }

                    edit_row_icon_code.onclick = () => {
                        edit_document_modal_fn();
                    };

                    elements.push(edit_row_element);
                    break;

            }
        }
        return elements;
    }


    async queryData() {

        if (this.fixed_data_source) {
            let filtered = this.fixed_data_source.filter((item) => {
                if (!this.query || this.query === "") {
                    return true;
                }
                let match = false;

                for (let prop in item) {
                    if ((item[prop].toString().toLowerCase().indexOf(this.query) !== -1 || item[prop].toString().toLowerCase() == this.query) && ((!this.filter_field || this.filter_field === "*") || (this.filter_field === prop))) {
                        match = true;
                    }
                }

                return match;
            });
            if (this.sort){
                for (const prop in this.sort){
                    filtered = filtered.sort((a,b)=>{

                        let field_type = TypeFromProperty(a[prop]);

                        if (this.sort[prop].order === "desc"){
                            if (field_type === "string") {
                                if (a[prop] < b[prop]) {
                                    return -1;
                                }
                                if (a[prop] > b[prop]) {
                                    return 1;
                                }
                                return 0;
                            }else if (field_type === "number"){
                                return (parseFloat(a[prop]) - parseFloat(b[prop]))
                            }else{
                                return a[prop] - b[prop];
                            }
                        }else{
                            if (field_type === "string") {
                                if (b[prop] < a[prop]) {
                                    return -1;
                                }
                                if (b[prop] > a[prop]) {
                                    return 1;
                                }
                                return 0;
                            }else if (field_type === "number"){
                                return (parseFloat(b[prop]) - parseFloat(a[prop]))
                            }else{
                                return b[prop] - a[prop];
                            }
                        }
                    });
                }
            }
            let page_number = Math.min(this.page_number, (filtered.length / this.elements_per_page) | 0);
            this.total_elements = filtered.length;
            return filtered.slice(page_number * this.elements_per_page, (page_number + 1) * this.elements_per_page);
        }

        const search_input = document.getElementById("smart-table-filter");

        search_input.className = search_input.className.replace(" error", "");

        let query = {match_all: {}};

        if (this.match_type === "query") {
            search_input.placeholder = `{ "simple_query_string" : { "query": "2018" } }`;

            try {
                if (this.query === "") {
                    return [];
                }
                query = JSON.parse(this.query);
            } catch (e) {
                if (search_input.className.indexOf("error") === -1) {
                    search_input.className += " error";
                }
                this.total_elements = 0;
                this.updatePagination();
                this.parsing_error = "Malformed Query DSL JSON";
                return [];
            }
        } else {

            search_input.placeholder = `Search`;

            if (this.headers && this.query && this.query !== "") {
                const fields = [];
                for (let prop of this.headers) {
                    if (prop !== "_id" && ((!this.filter_field || this.filter_field === "*") || (this.filter_field === prop))) {
                        fields.push(prop);
                    }
                }
                if (this.query === "true" || this.query === "false") {
                    query = {query_string: {query: (this.query === "true"), fields: fields}};
                } else {
                    if (this.match_type === "fuzzy") {

                        let q = this.query.replace(/[\W_]+/g, " ");

                        query = {query_string: {query: `*${q}*`, fields: fields, lenient: true}};
                    } else {

                        //if (fields.length === 1) {

                        // TODO implement exact match single field
                        //   query = {"query_string": {"query": this.query, "analyzer":"keyword"}}
                        //} else {
                        query = {
                            "multi_match": {
                                "query": this.query,
                                "analyzer": "keyword"
                            }
                        }
                        //}
                    }
                }
            }
        }

        this.page_number = Math.min(this.page_number, (this.total_elements / this.elements_per_page) | 0);

        let result;

        try {
            let ob = {
                index: this.collection_index.index,
                size: this.elements_per_page,
                from: this.page_number * this.elements_per_page,
                body: {
                    query: query
                }
            };
            if (this.sort){
                ob.body.sort = [this.sort];
                console.log("USING SORT", this.sort);
            }
            //console.log("SEARCH",ob);
            result = await this.app.elasticDriver.client.search(ob);
            this.parsing_error = null;
        } catch (e) {
            if (search_input.className.indexOf("error") === -1 && this.match_type === "query") {
                search_input.className += " error";
            }
            //  let err_mgs_element = document.getElementById("results-err");
            //  err_mgs_element.innerHTML = `<p class="error">${e.message}</p>`;
            this.parsing_error = e.message;

            console.log("PARSING ERROR", e)

            return [];
        }

        this.total_elements = result.hits.total.value;
        if (this.page_number !== Math.min(this.page_number, (this.total_elements / this.elements_per_page) | 0)) {
            this.page_number = 1;
            return await this.queryData();
        } else {
            this.page_number = Math.min(this.page_number, (this.total_elements / this.elements_per_page) | 0);
        }

        await this.updatePagination();

        return result.hits.hits.map((doc) => {
            return Object.assign({_id: doc._id}, flatObject(doc._source));
        })
    }

    subscribers = [];

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }

}

module.exports = ElasticSmartTable;
