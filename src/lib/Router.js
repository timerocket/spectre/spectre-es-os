class Router {
    elasticDriver;
    _controller;
    subscribers = [];

    constructor() {
        window.setInterval(async () => {
            await this.healthCheck()
        }, 10000);
    }

    route(controller) {
        if (this._controller) {
            this._controller.blur();
        }
        this._controller = controller;
        controller.render();
        controller.focus();
    }

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }
        this.subscribers[idx].forEach((fn) => {
            fn(data);
        });
    }

    async healthCheck() {
        if (this.elasticDriver) {
            if (this.elasticDriver.client) {
                const status = await this.elasticDriver.ping();

                if (!status) {
                    this.publish("disconnect");
                }
            }
        }
    }
}

module.exports = Router;