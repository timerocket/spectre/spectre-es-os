class Modal {
    element;
    modal_window;
    constructor(innerHTML, options={}) {
        this.element = document.createElement("div");
        this.element.className="flex modal-container";

        this.modal_window = document.createElement("div");
        this.modal_window.className = "modal-window";
        if (options){
            if (options.class){
                this.modal_window.className += " "+options.class;
            }
        }
        this.modal_window.innerHTML = innerHTML;

        this.element.appendChild(this.modal_window);

        document.body.appendChild(this.element);
    }

    close(){
        this.element.parentNode.removeChild(this.element);
    }
}

module.exports = Modal;