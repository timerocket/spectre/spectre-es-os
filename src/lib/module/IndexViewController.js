const ejs = require("ejs");
const fs = require("fs");
const filePath = require("../FilePath");
const Modal = require("../Modal");
const SmartTable = require("../ElasticSmartTable");
const validateIndexName = require("../ElasticIndexNameValidation");

class IndexViewController {

    app;
    smart_table;

    constructor(app) {
        this.subscribers = [];
        this.app = app;
    }

    async render() {
        let indices = await this.app.elasticDriver.getIndices();

        const tab_items = [{
            text: `<i class="far fa-window-close"></i> Disconnect`,
            id: "disconnect"
        }, {
            text: `<i class="far fa-plus-square"></i> Create Index`,
            id: "new-index",
            class: "right"
        }
        ];

        document.body.innerHTML =
            ejs.render(fs.readFileSync(filePath("../view/tabs.ejs")).toString(), {items: tab_items}, null) +
            ejs.render(fs.readFileSync(filePath("../view/indexes.ejs")).toString(), {
                uri: this.app.elasticDriver.uri,
                indices
            }, null);

        this.smart_table = this.smart_table || new SmartTable(this.app, indices, ["delete"]);
        this.smart_table.updateFixedDataSource(indices);
        this.smart_table.subscribers=[];

        this.smart_table.render(document.getElementById("index-table"));

        this.smart_table.subscribe("select", (index) => {
            this.publish("select-index", index);
        });

        this.smart_table.subscribe("refresh", async () => {
            let refresh_btn = document.getElementById("smart-table-refresh-rows");
            let refresh_icon = document.getElementById("refresh-icon");
            refresh_icon.className = "fas fa-spin fa-sync";
            refresh_btn.disabled="disabled";
            let indices = await this.app.elasticDriver.getIndices();
            this.smart_table.updateFixedDataSource(indices);
            await this.smart_table.clearTable();
            await this.smart_table.updateTable();
            refresh_btn.disabled=false;
            refresh_icon.className = "fas fa-sync";
        });

        this.smart_table.subscribe("delete", async (index) => {
            indices = indices.filter((ind) => {
                return ind.index !== index.index
            });
            this.smart_table.updateFixedDataSource(indices);
            await this.app.elasticDriver.client.indices.delete({
                index: index.index
            });
        });

        /*

        const tab_items = [{
            text:`<i class="far fa-window-close"></i> Disconnect`,
            id:"disconnect"
        }
        ];
        document.body.innerHTML =
            ejs.render(fs.readFileSync(filePath("../view/tabs.ejs")).toString(), {items: tab_items}, null)+
            ejs.render(fs.readFileSync(filePath("../view/indexes.ejs")).toString(), {uri:this.app.kafkaDriver.uri, indices}, null);

        for (let i = 0; i < indices.length; i++) {

            document.getElementById("index-" + i).onclick = () => {
                this.publish("select-index",indices[i]);
            }

        }

*/

        document.getElementById("disconnect").onclick = () => {
            this.publish("disconnect");
        }

        const new_index_modal_fn = (err, index_name) => {
            // this.publish("disconnect");
            let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/new-index.ejs")).toString(), {index:index_name,err: err || null}, null));

            let index_element = document.getElementById('index');
            let create_element = document.getElementById('create');
            let cancel_element = document.getElementById('cancel');

            index_element.onclick = () => {
                index_element.style.backgroundColor = "#60728b";
            }

            create_element.onclick = async () => {

                if (!index_element.value) {
                    index_element.style.backgroundColor = `#e25353`;
                    return false;
                }
                let idx_name = index_element.value;
                let index_name_check = validateIndexName(index_element.value);

                if (!index_name_check.status) {
                    modal.close();
                    console.error("INDEX CREATE ERROR", index_name_check.message);
                    new_index_modal_fn(index_name_check.message, index_element.value);
                    return;
                }


                modal.modal_window.style.textAlign = "center";
                modal.modal_window.style.color = "#c7d7ef";
                modal.modal_window.innerHTML = `<i class="fas fa-4x fa-spin fa-circle-notch"></i><h4>Creating Index...</h4>`;

                const exists = await this.app.elasticDriver.client.indices.exists({
                    index: index_element.value,
                });

                try {

                    if (!exists) {

                        const create_result = await this.app.elasticDriver.client.indices.create({
                            index: index_element.value,
                            body: {},
                            wait_for_active_shards: "1"
                        });

                      //  console.log("CREATE INDEX RESULT", create_result);
                    }


                    let check_created_interval = window.setInterval(async () => {

                        let indices = await this.app.elasticDriver.getIndices();
                        indices.forEach(async (ind) => {
                            if (ind.index === index_element.value) {
                                window.clearInterval(check_created_interval);
                                modal.close();
                                //this.publish("select-index", index_element.value);
                                return await this.render();
                            }
                        });


                    }, 100);

                } catch (e) {
                    modal.close();
                    console.error("CREATE ERROR", e);
                    new_index_modal_fn(e.message,idx_name);
                    return;
                }


            }

            cancel_element.onclick = () => {
                modal.close();
            }

        }

        document.getElementById("new-index").onclick = () => {
            new_index_modal_fn()
        };

        return null;
    }

    async focus() {


        return null;
    }

    async blur() {
        return null;
    }

    subscribers = [];

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }


}

module.exports = IndexViewController;
