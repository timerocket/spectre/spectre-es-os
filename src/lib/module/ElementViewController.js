const ejs = require("ejs");
const fs = require("fs");
const flatObject = require("../FlatObject");
const filePath = require("../FilePath");
const SmartTable = require("../ElasticSmartTable");
const Modal = require("../Modal");

class ElementViewController {

    app;
    index;
    smart_table = [];

    constructor(app) {
        this.app = app;
    }

    async render() {

        const tab_items = [{
            text: `<i class="far fa-caret-square-left"></i> View All Indices`,
            id: "back"
        }, {
            text: `<i class="far fa-plus-square"></i> Insert Document`,
            id: "insert-document",
            class: "right"
        }
        ];

        document.body.innerHTML =
            ejs.render(fs.readFileSync(filePath("../view/tabs.ejs")).toString(), {items: tab_items}, null)
            + ejs.render(fs.readFileSync(filePath("../view/elements.ejs")).toString(), {
                index: this.index
            }, null);


        if (!this.smart_table[this.index.index]) {
            this.smart_table[this.index.index] = new SmartTable(this.app, null, ["edit", "delete"], this.index);
        }
        const smart_table = this.smart_table[this.index.index];
        this.smart_table.subscribers=[];

        await smart_table.render(document.getElementById("index-table"));

        smart_table.subscribe("select", (doc) => {
            this.publish("select-row", doc);
        });

        smart_table.subscribe("refresh", async () => {
            let refresh_btn = document.getElementById("smart-table-refresh-rows");
            let refresh_icon = document.getElementById("refresh-icon");
            refresh_icon.className = "fas fa-spin fa-sync";
            refresh_btn.disabled="disabled";
            await smart_table.clearTable();
            await smart_table.updateTable();
            refresh_btn.disabled=false;
            refresh_icon.className = "fas fa-sync";
        });

        smart_table.subscribe("edit", async (doc) => {
            const update_id = doc._id;
            delete doc._id;

            const result = await this.app.elasticDriver.client.update({
                index: this.index.index,
                id: update_id,
                body: {
                    doc: doc
                }
            });

        })

        smart_table.subscribe("delete", async (doc) => {

            const result = await this.app.elasticDriver.client.delete({
                index: this.index.index,
                id: doc._id,
                refresh: true
            });


        });


           const insert_document_modal_fn = (err, doc) => {

            let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/insert-document.ejs")).toString(), {err:err||null,doc:doc||null}, null),{class:"wide"});

            let document_element = document.getElementById('document');
            let insert_element = document.getElementById('insert');
            let cancel_element = document.getElementById('cancel');

            document_element.onclick = () => {
                document_element.style.backgroundColor = "#2f3a4b";
            }

               let valid_json=false;
            insert_element.onclick = async () => {

                let initial_value = document_element.value;

                let insert_json_data;
                try {
                    insert_json_data = JSON.parse(initial_value);
                    valid_json = true;
                }catch(e){

                }

                if (!document_element.value || !valid_json) {
                    modal.close();
                    insert_document_modal_fn("Malformed JSON Document",initial_value);
                    let document_element = document.getElementById('document')
                    document_element.style.backgroundColor = `#e25353`;
                    return false;
                }
                modal.modal_window.style.textAlign = "center";
                modal.modal_window.style.color = "#c7d7ef";
                modal.modal_window.innerHTML = `<i class="fas fa-4x fa-spin fa-circle-notch"></i><h4>Inserting Document${(insert_json_data instanceof Array)?"s":""}...</h4>`;

                let confirm_doc;

                try {
                    if (insert_json_data instanceof Array) {
                        confirm_doc = insert_json_data[0];
                        await this.app.elasticDriver.client.bulk({
                            index: this.index.index,
                            body: insert_json_data.flatMap(doc => [{index: {_index: this.index.index}}, doc]),
                        });
                    } else {
                        confirm_doc = insert_json_data;
                        await this.app.elasticDriver.client.index({
                            index: this.index.index,
                            body: insert_json_data,
                        });
                    }
                }catch(e){
                    modal.close();
                    console.error("INSERT ERROR",e);
                    insert_document_modal_fn(e.message, initial_value);
                    return;
                }

                if (!confirm_doc){
                    modal.close();
                    console.error("MALFORMED DOCUMENT?",e);
                    insert_document_modal_fn("Malformed JSON Document",initial_value);
                    return;
                }

                let check_created_interval = window.setInterval(async () => {

                    let fields = [];
                    let flat_document = flatObject(confirm_doc);
                    for (let prop in flat_document){
                        fields.push(prop);
                    }

                    let search_document = {};
                    search_document[fields[0]] = flat_document[fields[0]];

                    let results = await this.app.elasticDriver.client.search({
                        index: this.index.index,
                        body: {
                            query: {
                                match: search_document,
                            },
                        },
                    });

                    if (results.hits.hits.length >= 1){
                        window.clearInterval(check_created_interval);
                        modal.close();
                        return await this.render();
                    }

                }, 100);


            }

            cancel_element.onclick = () => {
                modal.close();
            }

        }

        document.getElementById("insert-document").onclick = ()=>{insert_document_modal_fn();};


        document.getElementById("back").onclick = () => {
            this.publish("back")
        }

        return null;
    }

    async focus() {

        return null;
    }

    async blur() {
        return null;
    }

    setIndex(idx) {
        this.index = idx;
    }

    subscribers = [];

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }

}

module.exports = ElementViewController;
