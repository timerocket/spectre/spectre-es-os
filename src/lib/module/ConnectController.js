const ejs = require("ejs");
const fs = require("fs");
const ElasticDriver = require("../ElasticDriver");
const filePath = require("../FilePath");

class ConnectController {

    app;
    elasticDriver;
    subscribers = [];
    use_saved = {};

    constructor(app) {
        this.subscribers = [];
        this.app = app;
        this.elasticDriver = new ElasticDriver();
    }

    async render() {

        document.body.innerHTML = ejs.render(fs.readFileSync(filePath("../view/connect.ejs")).toString(), Object.assign(this.elasticDriver.getConnectionDetails(), this.use_saved), null);
        //this.use_saved = {};
        return null;
    }

    async focus() {

        await this.checkURIStatus();

        const uri = document.getElementById("uri");
        const username = document.getElementById("username");
        const password = document.getElementById("password");
        const apiKey = document.getElementById("apiKey");
        username.onkeyup = username.onchange = username.onclick =
            password.onkeyup = password.onchange = password.onclick =
                apiKey.onkeyup = apiKey.onchange = apiKey.onclick =
                    uri.onkeyup = uri.onchange = uri.onclick = async () => {
                        await this.checkURIStatus();
                    }

        let connect = document.getElementById('connect');

        let save_connection_checkbox = document.getElementById("save_connection");
        let save_connection_nickname = document.getElementById("save_connection_nickname");

        save_connection_checkbox.onchange = () => {
            save_connection_nickname.style.display = (save_connection_checkbox.checked) ? "inline-block" : "none";
        }

        connect.onclick = async () => {

            let checked_save = save_connection_checkbox.checked;

            this.publish('check-status');
            const result = await this.elasticDriver.connectURI(document.getElementById("uri").value);

            if (!result.status) {
                this.publish('check-status-bad', result.message);
                this.publish('uri-bad');
            } else {
                this.publish('uri-reset');
                this.publish('connect');
                if (checked_save) {
                    this.elasticDriver.addRecentConnection();
                }
            }

        }

        let recent = this.elasticDriver.recentConnections;

        if (recent && recent.length) {
            for (let i = 0; i < recent.length; i++) {
                let el = document.getElementById("recent-connection-" + i);
                el.onclick = async () => {
                    this.use_saved = recent[i];
                    await this.publish("refresh");
                    const result = await this.elasticDriver.connectURI(this.use_saved.uri, this.use_saved);
                    if (result.status) {
                        this.publish('connect');
                    } else {
                        this.publish("uri-bad");
                    }

                    // this.publish("refresh");
                }

                let rm = document.getElementById("remove-recent-connection-" + i);
                rm.onclick = () => {
                    this.elasticDriver.recentConnections.splice(i, 1);
                    this.elasticDriver.saveRecentConnections();
                    this.publish("refresh");
                }
            }
        }


        return null;
    }

    async checkURIStatus() {

        this.publish('check-status');
        this.publish('uri-reset');

        const check = await this.elasticDriver.testConnectionURI(document.getElementById("uri").value);
        if (check.status) {
            this.publish('check-status-good');
        } else {
            this.publish('check-status-bad', check.message);
        }

    }

    async blur() {
        return null;
    }

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }
}

module.exports = ConnectController;