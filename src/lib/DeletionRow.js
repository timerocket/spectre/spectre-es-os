const unflattenObject = require("./UnflattenObject");

class DeletionRow {
    source;

    constructor(source) {
        this.source = source;
    }

    getElement(available_actions, headers) {
        const row_elem = document.createElement("tr");
        row_elem.className = "deleting";
        let action_container_td = document.createElement("td");
        action_container_td.className= "actions";
        action_container_td.colSpan = available_actions.length;
        let action_container_td_table = document.createElement("table");
        let action_container_td_table_tr = document.createElement("tr");

        const done_elem = document.createElement("span");

        done_elem.style.marginRight="0.5em";
        done_elem.innerHTML = `<i class="far fa-fw fa-trash-alt"></i>`;

        done_elem.onclick = async (e) => {
            e.preventDefault();
            e.cancelBubble = true;
            this.publish("delete", this.source);
        };

        const cancel_elem = document.createElement("span");
        cancel_elem.style.marginLeft="0.5em";
        cancel_elem.innerHTML = `<i class="fas fa-fw fa-undo"></i>`;

        cancel_elem.onclick = async (e) => {
            e.preventDefault();
            e.cancelBubble = true;
            this.publish("cancel");
        };

        if (available_actions.indexOf("view") !== -1) {
            const view_elem = document.createElement("td");
            view_elem.className = "clickable";
            view_elem.innerHTML = `<i class="far fa-fw fa-file-alt"></i>`;
            view_elem.onclick = async (e) => {
                let modal = new Modal(ejs.render(fs.readFileSync(filePath("../view/view-document.ejs")).toString(), {doc: unflattenObject(this.source)}, null),{class:"wide"});
                let close_element = document.getElementById('cancel');
                close_element.onclick = () => {
                    modal.close();
                }
            }

            action_container_td_table_tr.appendChild(view_elem);
        }

        if (available_actions.indexOf("edit") !== -1) {
            action_container_td_table_tr.appendChild(document.createElement("td"))
        }

        let elem_actions_container = document.createElement("td");
        elem_actions_container.className = "padded";
        elem_actions_container.className = "clickable";
        elem_actions_container.style.whiteSpace = "pre";
        elem_actions_container.appendChild(done_elem);
        elem_actions_container.appendChild(cancel_elem)

        action_container_td_table_tr.appendChild(elem_actions_container);

        action_container_td_table.append(action_container_td_table_tr);
        action_container_td.append(action_container_td_table);
        row_elem.append(action_container_td);

        this.source_value_elements = [];

        for (const prop of headers) {

            const edit_row_element = document.createElement("td");
            edit_row_element.className = "padded";
            edit_row_element.innerText = this.source[prop]||"";
            this.source_value_elements[prop] = edit_row_element;
            row_elem.appendChild(edit_row_element);

        }


        return row_elem;
    }

    // Thanks to https://stackoverflow.com/users/4620771/nenad-vracar
    rollupDocument() {
        var result = {}
        for (var i in this.source) {
            var keys = i.split('.')
            keys.reduce((r, e, j) => {
                return r[e] || (r[e] = isNaN(Number(keys[j + 1])) ? (keys.length - 1 == j ? this.source[i] : {}) : [])
            }, result)
        }
        return result
    }

    subscribers = [];

    subscribe(idx, fn) {
        if (!this.subscribers[idx]) {
            this.subscribers[idx] = [];
        }
        this.subscribers[idx].push(fn);
    }

    async publish(idx, data) {
        if (!this.subscribers[idx]) {
            return;
        }

        return new Promise(async (resolve, reject) => {

            for (const fn of this.subscribers[idx]) {
                await fn(data);
            }
            resolve();
        });

    }
}

module.exports = DeletionRow;