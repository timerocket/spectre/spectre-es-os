require('dotenv').config();
const { notarize } = require('electron-notarize');

exports.default = async function notarizing(context) {
    const {electronPlatformName, appOutDir} = context;
    if (electronPlatformName !== 'darwin') {
        return;
    }

    const appName = context.packager.appInfo.productFilename;

    return await notarize({
      appBundleId    : '8V8XY9T2G7.com.timerocket.spectre',
      appPath        : `${appOutDir}/${appName}.app`,
      appleId        : process.env.APPLE_USERNAME,
      appleIdPassword: process.env.APPLE_PASSWORD,
    });
};