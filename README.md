# Spectre ES (Elasticsearch UI & Editor)

Have you ever been working in dev environment and wanted to update an Elasticsearch document without all of the hassle?

## Out of all the terrible Elasticsearch Desktop Client GUIs, this one's the best

Editing document fields happens in real-time, there is no save button, there is no undo. Do not sue me.

_Please don't use this in production._

### Running from source
`yarn install` \
`yarn start`

### Building local target from source
`yarn make` Builds Binary for local target \
`yarn make:all` Builds Binary for all configured targets (darwin, linux, win32) 

#### Building all targets from source Dependencies (Mac OSX)
`brew install fakeroot dpkg rpm` \
`brew cask install xquartz` \
`brew install homebrew/cask-versions/wine-devel` \
`brew install mono`



### Download prebuilt Binaries
[Releases](https://github.com/arkamedus/spectre-elasticsearch-gui/releases)


### Future Features
Save on demand,\
A comprehensible UI
