
Update `entitlements.**` in `build/` with appropriate information:

Get your `embedded.provisionprofile` into the root directory from appstoreconnect.apple.com for your app.

Ensure you have the following certs added into your keychain:

`Developer ID Application: <Team name> (<Team Id>)` \
`3rd Party Mac Developer Installer: <Team name> (<Team Id>)` 

#### Run Locally
`yarn start`

#### Make for Mac App Store
`bash package.sh` \
Upload `signed-release/**.pkg` to Transporter (from Mac App Store) to be uploaded as a build for your Application.