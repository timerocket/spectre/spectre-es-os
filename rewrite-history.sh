#!/usr/bin/env bash

commits=$(git rev-list --all --remotes)
for i in $commits; do
    git checkout "$i"
    jq '.license = "UNLICENSED"' package.json > tmp.json
    mv tmp.json package.json
    git add package.json
    git commit --amend --no-edit
done