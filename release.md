# Distributing this app

you need in your keychain

- Developer ID Application Certificate
- 3rd Party Mac Developer Application

# Provisioning profiles
There must be a different provisioning profile for 
- MAC / DMG (`mac.provisionprofile`)
- MAS (`mas.provisionprofile`)

https://github.com/electron-userland/electron-builder/issues/4960
For MAS you must manually update the property `mac.provisioningProfile` and set to `mas.provisionprofile` or it won't work

## Transporter App

- Must be signed in with apple@timerocket.com
- Select the `.pkg` file inside the `release/mas` folder